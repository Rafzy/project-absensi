const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const fileUpload = require('express-fileupload')
const favicon = require('serve-favicon')
const cookieParser = require('cookie-parser')
const helmet = require('helmet')
const morgan = require('morgan')
const app = express()

// Basic Set-up
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')
app.use(fileUpload())
app.use(helmet.frameguard())
app.use(helmet.xssFilter())
app.use(helmet.noSniff())
app.disable('x-powered-by')
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

// Logger
app.use(morgan('[Method: :method] :remote-user [Date: :date[web]] - [statusCode: :status] - [url: :url] - [speedResponse: :response-time]'))

// Error Handling 404
app.use((err, res, next) => {
  if (err) {
    res.send('Error 404')
  }
})

// Create Server
app.listen(8080, () => {
  console.log('Server running on http://localhost:8080')
})
